import { UnibankPage } from './app.po';

describe('unibank App', () => {
  let page: UnibankPage;

  beforeEach(() => {
    page = new UnibankPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to ub!!');
  });
});
