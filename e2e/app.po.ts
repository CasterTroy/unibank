import { browser, by, element } from 'protractor';

export class UnibankPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('ub-root h1')).getText();
  }
}
