import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DashboardComponent } from './content/dashboard/dashboard.component';
import { SummaryComponent } from './content/dashboard/summary/summary.component';

import { HistoryComponent } from './content/dashboard/history/history.component';
import { ChangesComponent } from './content/dashboard/changes/changes.component';
import { MessagesComponent } from './content/dashboard/messages/messages.component';
import { routing } from "./app.routing";
import { AccountsComponent } from './content/accounts/accounts.component';
import { SevingsComponent } from './content/sevings/sevings.component';
import { LoansComponent } from './content/loans/loans.component';
import { InvestmensComponent } from './content/investmens/investmens.component';
import { ContractsComponent } from './content/contracts/contracts.component';
import { HeadComponent } from './head/head.component';
import { NavigationComponent } from './navigation/navigation.component';
import { CardsComponent } from "./content/cards/cards.component";
import { CardComponent } from './content/dashboard/card/card.component';

import { AppService } from './app.service';
import { CardService } from "./content/dashboard/card/card.service";
import {HttpModule} from "@angular/http";
import {HistoryService} from "./content/dashboard/history/history.service";
import {ChargesService} from "./content/dashboard/changes/charges.service";
import {MessagesService} from "./content/dashboard/messages/messages.service";


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SummaryComponent,
    CardsComponent,
    HistoryComponent,
    ChangesComponent,
    MessagesComponent,
    AccountsComponent,
    SevingsComponent,
    LoansComponent,
    InvestmensComponent,
    ContractsComponent,
    HeadComponent,
    NavigationComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpModule
  ],
  providers: [AppService,
              CardService,
              HistoryService,
              ChargesService,
              MessagesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
