import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SevingsComponent } from './sevings.component';

describe('SevingsComponent', () => {
  let component: SevingsComponent;
  let fixture: ComponentFixture<SevingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SevingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SevingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
