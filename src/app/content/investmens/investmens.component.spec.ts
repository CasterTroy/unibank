import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestmensComponent } from './investmens.component';

describe('InvestmensComponent', () => {
  let component: InvestmensComponent;
  let fixture: ComponentFixture<InvestmensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestmensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestmensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
