import { Injectable } from '@angular/core';
import {AppService} from "../../../app.service";
import {environment} from "../../../../environments/environment";

@Injectable()
export class HistoryService {

  constructor(private appService: AppService) { }

  getHistory(card){
    return this.appService.get(environment.historyUrl + card +'.json')
  }

}
