import { Component, OnInit } from '@angular/core';
import {HistoryService} from "./history.service";
declare var  $ : any;
@Component({
  selector: 'ub-history',
  templateUrl: 'history.component.html',
  styleUrls: ['history.component.sass']
})
export class HistoryComponent implements OnInit {
  private _history: any;
  constructor(private historyService : HistoryService) {
    this.historyService.getHistory('tuday').subscribe(data=>{
      this._history = data.list;
      console.log(data.list)
    })
  }

  ngOnInit() {
    $('.link-h').on('click', function () {
      $('.link-h').removeClass('active')
      $(this).addClass('active')
    })
  }

  getHistoryTuday(){
    this.historyService.getHistory('tuday').subscribe(data=>{
      this._history = data.list;
    })
  }
  getHistoryYesterday(){
    this.historyService.getHistory('yesterday').subscribe(data=>{
      this._history = data.list;
    })
  }
}
