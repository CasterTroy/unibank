import { Injectable } from '@angular/core';
import {AppService} from "../../../app.service";
import {environment} from "../../../../environments/environment";
@Injectable()
export class ChargesService {

  constructor(private appService: AppService) { }

  getCharges(charges){
    return this.appService.get(environment.chargesUrl + charges +'.json')
  }
}
