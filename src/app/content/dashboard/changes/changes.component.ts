import { Component, OnInit } from '@angular/core';
import {ChargesService} from "./charges.service";

declare var google: any;
declare var  $ : any;
@Component({
  selector: 'ub-changes',
  templateUrl: 'changes.component.html',
  styleUrls: ['changes.component.sass']
})
export class ChangesComponent implements OnInit {
  private _charges: any;


  constructor(private chargesService : ChargesService) { }

  ngOnInit() {
    $('.link-c').on('click', function () {
      $('.link-c').removeClass('active')
      $(this).addClass('active')
    })

    this.chargesService.getCharges('week').subscribe(data=>{
      this._charges = data;
      this._init(data)
    })
  }

  private _init(data){
    google.charts.load("current", {packages:["corechart"]});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
      var tmp = google.visualization.arrayToDataTable([
        ['Task', 'Hours per Day'],
        ['Food',      +data.food],
        ['House',     +data.house],
        ['Car',       +data.car],
        ['Bills',     +data.bills],
      ]);

      var options = {
        height: 250,
        pieHole: 0.95,
        colors: ['#86157b', '#70ce09', '#ff2366', '#f3b49f', '#75bbc3'],
        legend: 'none'
      };

      var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
      chart.draw(tmp, options);
    }
  }

  getWeek(){
    this.chargesService.getCharges('week').subscribe(data=>{
      this._charges = data;
      this._init(data)
    })
  }
  getMonth(){
    this.chargesService.getCharges('month').subscribe(data=>{
      this._charges = data;
      this._init(data)
    })
  }
}
