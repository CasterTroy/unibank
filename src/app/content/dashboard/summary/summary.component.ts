import { Component, OnInit } from '@angular/core';

declare var google: any;

@Component({
  selector: 'ub-summary',
  templateUrl: 'summary.component.html',
  styleUrls: ['summary.component.sass']
})
export class SummaryComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ['Month', 'Expenses'],
        ['Mon',  4000],
        ['Tue',  3000],
        ['Wed',  2555],
        ['Thu',  6000],
        ['Fri',  4500],
        ['Sat',  7000],
        ['Sun',  7500],
      ]);

      var options = {
        curveType: 'function',
        legend: 'none',
        series: {
          0: { color: '#922c88' },
        },
        lineWidth: 3,
        vAxis: {
          ticks: [0, 2000, 4000, 6000, 8000]
        }
      };

      var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

      chart.draw(data, options);
    }



  }

}
