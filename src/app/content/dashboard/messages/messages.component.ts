import { Component, OnInit } from '@angular/core';
import {MessagesService} from "./messages.service";
declare var $: any
@Component({
  selector: 'ub-messages',
  templateUrl: 'messages.component.html',
  styleUrls: ['messages.component.sass']
})
export class MessagesComponent implements OnInit {
  private _users: any;
  constructor(private messagesService : MessagesService) { }

  ngOnInit() {
    var item = $('.list>li');

    $('.list').on('click', '.list__item', function () {
      $('.list>li').removeClass('active');
      $(this).addClass('active');
    })


    this.messagesService.getMessages().subscribe(data=>{
      this._users = data.list;
    })


  }

}
