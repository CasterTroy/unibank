import { Injectable } from '@angular/core';
import {AppService} from "../../../app.service";
import {environment} from "../../../../environments/environment";

@Injectable()
export class MessagesService {

  constructor(private appService: AppService) { }

  getMessages(){
    return this.appService.get(environment.messagesUrl +'messages.json')
  }
}
