import { Injectable } from '@angular/core';
import {AppService} from "../../../app.service";
import {environment} from "../../../../environments/environment";

@Injectable()
export class CardService {

  constructor(private appService: AppService) { }

  getCard(history){
    return this.appService.get(environment.cardUrl + history +'.json')
  }

}
