import { Component, OnInit } from '@angular/core';
import { CardService } from "./card.service";

@Component({
  selector: 'ub-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.sass']
})
export class CardComponent implements OnInit {
  private _card: any;
  private _status: boolean = false;
  constructor(private cardService : CardService) { }

  ngOnInit() {
    this.cardService.getCard('card-0000-0000-0000-0000').subscribe(data=>{
      this._card = data;
      console.log(data.status)
      data.status.toLowerCase() == 'active' ? this._status = true: this._status = false;
    })
  }

  public getCard(card){
    this.cardService.getCard(card).subscribe(data=>{
      console.log(data);
      data.status.toLowerCase() == 'active' ? this._status = true: this._status = false;
      this._card = data;
    })
  }

}
