/**
 * Created by JackYurchenco on 30.06.2017.
 */
import {Routes, RouterModule} from "@angular/router";
import {DashboardComponent} from "./content/dashboard/dashboard.component";
import {AccountsComponent} from "./content/accounts/accounts.component";
import {ContractsComponent} from "./content/contracts/contracts.component";
import {InvestmensComponent} from "./content/investmens/investmens.component";
import {LoansComponent} from "./content/loans/loans.component";
import {SevingsComponent} from "./content/sevings/sevings.component";
import {CardsComponent} from "./content/cards/cards.component";


const APP_ROUTES: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'accounts', component: AccountsComponent},
  {path: 'contracts', component: ContractsComponent},
  {path: 'cards', component: CardsComponent},
  {path: 'investmens', component: InvestmensComponent},
  {path: 'loans', component: LoansComponent},
  {path: 'sevings', component: SevingsComponent},

];

export const routing = RouterModule.forRoot(APP_ROUTES);
